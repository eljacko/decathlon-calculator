module.exports = {
    devServer: {
        overlay: {
            warnings: false,
            errors: true
        },
        port: 3000,
        proxy: 'http://localhost:3000',
    }
}
