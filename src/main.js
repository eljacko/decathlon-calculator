import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/black-green-light.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = false

axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';


Vue.use(VueRouter)
Vue.use(VueMaterial)
Vue.use(VueAxios, axios)

const router = new VueRouter({
})

window.vue = new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
